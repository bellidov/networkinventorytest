package util.dao;

import model.ItemData;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.util.List;

/**
 * Created by Xrup on 12.02.2015.
 */
public class ItemDataDaoImpl implements ItemDataDao {
    @Override
    public boolean addItem(ItemData itemData) {
        Session session = null;
        try{
            SessionFactory factory = HibernateUtil.getSessionFactory();
            session = factory.openSession();
            session.beginTransaction();
            session.save(itemData);
            session.getTransaction().commit();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if((session != null) && (session.isOpen())){
                session.close();
            }

        }

        return true;
    }

    @Override
    public boolean deleteItem(ItemData book) {
        return false;
    }

    @Override
    public ItemData getItem(int id) {
        return null;
    }

    @Override
    public List<ItemData> getAllItems() {
        Session session = null;
        List<ItemData> itemDataList = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            itemDataList = session.createCriteria(ItemData.class).list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if((session != null) && (session.isOpen())){
                session.close();
            }

        }
        return itemDataList;
    }
}
