package treeView.client.beans;


import model.ItemData;

import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 * Created by Xrup on 12.02.2015.
 */
@Entity
@XmlRootElement
public class City implements ItemData {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column (name="name")
    private String name;

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }


    public int getId() {
        return id;
    }

    @XmlAttribute
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }
}
