package controller;

import model.beans.City;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by Xrup on 12.02.2015.
 */
public class ServletTreeView  extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = req.getRequestDispatcher("TreeView.html");
        dispatcher.forward(req, resp);
    }
}
