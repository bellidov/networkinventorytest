package model.beans;

import model.ItemData;

import javax.persistence.*;

/**
 * Created by Xrup on 12.02.2015.
 */
@Entity
public class Switch implements ItemData{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column (name="name")
    private String name;

    public void setName(String name) {
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }
}