package util.dao;

import model.ItemData;

import java.util.List;

/**
 * Created by Xrup on 12.02.2015.
 */
public interface ItemDataDao {
    boolean addItem(ItemData itemData);
    boolean deleteItem(ItemData itemData);
    ItemData getItem(int id);
    List<ItemData> getAllItems();
}
