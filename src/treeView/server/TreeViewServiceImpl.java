package treeView.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import model.ItemData;
import treeView.client.TreeViewService;
import util.Factory;
import util.dao.ItemDataDao;

import java.util.List;

public class TreeViewServiceImpl extends RemoteServiceServlet implements TreeViewService {
    // Implementation of sample interface method
    public String getMessage(String msg) {
        ItemDataDao dao = Factory.getInstance().getItemDataDao();
        List<ItemData> items = dao.getAllItems();

        return "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\" " + items.get(0).getName();
    }
}