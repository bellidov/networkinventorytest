package treeView.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("TreeViewService")
public interface TreeViewService extends RemoteService {
    // Sample interface method of remote interface
    String getMessage(String msg);

    /**
     * Utility/Convenience class.
     * Use TreeViewService.App.getInstance() to access static instance of TreeViewServiceAsync
     */
    public static class App {
        private static TreeViewServiceAsync ourInstance = GWT.create(TreeViewService.class);

        public static synchronized TreeViewServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
