package controller;

import model.beans.City;
import util.Factory;
import util.dao.ItemDataDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;

/**
 * Created by Xrup on 12.02.2015.
 */
public class ServletAddItem extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ItemDataDao itemDataDao = Factory.getInstance().getItemDataDao();
        City city = new City();
        city.setName(req.getParameter("name"));
        itemDataDao.addItem(city);


        try {

            File file = new File("C:\\file.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(City.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(city, file);
      //      jaxbMarshaller.marshal(city, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        //    RequestDispatcher dispatcher = req.getRequestDispatcher("ServletViewAll.class");
        //  dispatcher.forward(req, resp);
    }
}
