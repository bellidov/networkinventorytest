package treeView.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TreeViewServiceAsync {
    void getMessage(String msg, AsyncCallback<String> async);
}
