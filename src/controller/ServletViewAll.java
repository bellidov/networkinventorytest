package controller;

import model.ItemData;
import util.Factory;
import util.dao.ItemDataDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Xrup on 12.02.2015.
 */
public class ServletViewAll extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ItemDataDao itemDataDao = Factory.getInstance().getItemDataDao();

        List<ItemData> itemDataList = itemDataDao.getAllItems();

        ServletContext context = req.getServletContext();
        context.setAttribute("itemDataList", itemDataList);
        RequestDispatcher dispatcher = req.getRequestDispatcher("result.jsp");
        dispatcher.forward(req, resp);
    }
}
