package util;

import util.dao.ItemDataDao;
import util.dao.ItemDataDaoImpl;

/**
 * Created by Xrup on 12.02.2015.
 */
public class Factory {

    private static Factory instance = new Factory();
    private ItemDataDao itemDataDao;// = new BookDaoImpl();

    private Factory(){

    }

    public static Factory getInstance(){
        return instance;
    }

    public ItemDataDao getItemDataDao(){
        if(itemDataDao == null) {
            itemDataDao = new ItemDataDaoImpl();
        }
        return itemDataDao;
    }
}
