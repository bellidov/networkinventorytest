package util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by Xrup on 12.02.2015.
 */
public class HibernateUtil {
    private  static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    private HibernateUtil(){

    }

    static {
        Configuration configuration = new Configuration().configure();

        // loads configuration and mappings
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

        // builds a session factory from the service registry
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }
}
